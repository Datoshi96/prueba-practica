import React from 'react';
import Button from './Button';
import PropTypes from 'prop-types';

class Contact extends React.Component{
    state = {
        id: '',
        nombre: '',
        telefono: '',
        fechaNacimiento: '',
        direccion: '',
        correoElectronico: ''
    }
    handleChange = (event)=>{
        this.setState({
            [event.target.id]: event.target.value
        });
    }

    handleSave = (event)=>{
        event.preventDeault();
        if(this.state.nombre.trim() !== '' &&
            this.state.telefono.trim() !== '' &&
            this.state.fechaNacimiento.trim() !== '' &&
            this.state.direccion.trim() !== '' &&
            this.state.correoElectronico.trim() !== ''){
                this.props.onSave(this.state);

                this.setState({
                    nombre: '',
                    telefono: '',
                    fechaNacimiento: '',
                    direccion: '',
                    correoElectronico: ''
                })
            }
    }
    render() {
        return(
            <form onSubmit={this.handleSave}>
                <div className="form-group">
                    <label htmlFor="id">Nombre:</label>
                    <input id="id" name="id" type="text" readOnly className="form-control" value={props.idContact}
                    onChange={this.handleChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="nombre">Nombre:</label>
                    <input id="nombre" name="nombre" type="text" className="form-control"
                    placeholder="Ingrese el nombre del contacto" value={this.state.nombre}
                    onChange={this.handleChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="telefono">Teléfono:</label>
                    <input id="telefono" name="telefono" type="number" className="form-control"
                    placeholder="Ingrese el número del contacto" value={this.state.telefono}
                    onChange={this.handleChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="fechaNacimiento">Fecha Nacimiento:</label>
                    <input id="fechaNacimiento" name="fechaNacimiento" type="date" className="form-control" 
                    value={this.state.fechaNacimiento}  onChange={this.handleChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="direccion">Dirección:</label>
                    <input id="direccion" name="direccion" type="text" className="form-control"
                    placeholder="Ingrese la dirección del contacto" value={this.state.direccion}
                    onChange={this.handleChange}/>
                </div>

                <div className="form-group">
                    <label htmlFor="correo">Correo Electronico:</label>
                    <input id="correo" name="correo" type="text" className="form-control"
                    placeholder="Ingrese el correo del contacto" value={this.state.correoElectronico}
                    onChange={this.handleChange}/>
                </div>
                <Button type="submit" disabled={this.state.nombre.trim() === '' ||
                     this.state.telefono.trim() === ''|| this.state.fechaNacimiento.trim() === ''
                     || this.state.direccion.trim() === '' || this.state.correoElectronico.trim() === ''}
                     className="btn btn-primary">Guardar</Button>
            </form>
        );
    }

}
export default Contact;

Contact.propTypes = {
    idContact: PropTypes.number
}