import React, {Component} from "react";

function Search(props) { 
    return(
        <input type="text" placeholder="Buscar Contacto" value={props.searchText} className="form-control"
        onChange={props.onSearch}   />
    );
}

export default Search;