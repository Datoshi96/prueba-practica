import React from 'react';
import { Modal, ModalBody, Table, ModalFooter } from 'react-bootstrap';
import ModalHeader from 'react-bootstrap/esm/ModalHeader';
import DataTable from 'react-data-table-component';
import './App.css';
import Button from './components/Button';
import { FormFeedback, Input } from 'reactstrap';

const listTableBody = [
  {id: 1,nombre: 'David Garzon',  telefono: '3012345678',  fechaNacimiento: '1996-06-19',  direccion: 'Calle 123',  correoElectronico: 'd@g.c'}
];

class App extends React.Component {

  state = {
    listTableBody: listTableBody,
    contactos: {
      id: '',nombre: '',  telefono: '',  fechaNacimiento: '',  direccion: '',  correoElectronico: ''
    },
    mostrarActualizar: false,
    mostrarAgregar: false,
    listNew: [],
    listCopy: [],
    busqueda: '',
    listContactos: [],
    mensajeInvalidNombre: '',
    mensajeInvalidTelefono: '',
    mensajeInvalidFechaNacimiento: '',
    mensajeInvalidDireccion: '',
    mensajeInvalidCorreo: '',
    mensajeInvalidCorreoRequerido: '',
    invalidNombre: false,
    invalidTelefono: false,
    invalidFechaNacimiento: false,
    invalidDireccion: false,
    invalidCorreo: false,
    invalidCorreoRequerido: false,
  };
  

  handleSave = () => {
    if(this.state.contactos.nombre === ''){
      this.setState({invalidNombre: true, mensajeInvalidNombre: 'El campo es requerido'})
    }else{
      this.setState({invalidNombre: false})
    } 
    if (this.state.contactos.telefono === ''){
      this.setState({invalidTelefono: true, mensajeInvalidTelefono: 'El campo es requerido'})
    }else{
      this.setState({invalidTelefono: false})
    }
    if(this.state.contactos.fechaNacimiento === ''){
      this.setState({invalidFechaNacimiento: true, mensajeInvalidFechaNacimiento: 'El campo es requerido'})
    }else{
      this.setState({invalidFechaNacimiento: false})
    }
    if(this.state.contactos.direccion === ''){
      this.setState({invalidDireccion: true, mensajeInvalidDireccion: 'El campo es requerido'})
    }else{
      this.setState({invalidDireccion: false})
    }
    if(this.state.contactos.correoElectronico === '' ){
      this.setState({invalidCorreoRequerido: true, mensajeInvalidCorreoRequerido: 'El campo es requerido'})
      return false;
    }else{
      this.setState({invalidCorreoRequerido: false})
    }
    debugger
    if(!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(this.state.contactos.correoElectronico)){
      this.setState({invalidCorreo: true, mensajeInvalidCorreo: 'Formato de correo invalido'});
      return false;
    }else{
      this.setState({invalidCorreo: false})
    }
    var nuevoContacto = {...this.state.contactos};
    nuevoContacto.id = this.state.listTableBody.length+1;
    var list = this.state.listTableBody;
    list.push(nuevoContacto);
    this.setState({
      listTableBody: list,
      listCopy: this.state.listTableBody,
      mostrarAgregar: false
    });
  }

  handleChange = (event) => {
    this.setState({
      contactos:{
        ...this.state.contactos,
        [event.target.name]: event.target.value,
      },
    });
  }

  showModalUpdate = (dato) => {
    this.setState({mostrarActualizar: true, contactos: dato});
  }

  hideModalUpdate = () => {
    this.setState({mostrarActualizar: false});
  }

  showModalAdd = () => {
    this.setState({mostrarAgregar: true});
  }

  hideModalAdd = () => {
    this.setState({mostrarAgregar: false});
  }

  update = (contacto) => {
    debugger
    var i = 0;
    var list = this.state.listTableBody;
    list.map((campo) => {
      if(contacto.id === campo.id){
        debugger
        list[i].nombre = contacto.nombre;
        list[i].telefono = contacto.telefono;
        list[i].fechaNacimiento = contacto.fechaNacimiento;
        list[i].direccion = contacto.direccion;
        list[i].correoElectronico = contacto.correoElectronico;
      }
      i++;
    });
    this.setState({
      listContactos: list, listTableBody: list, mostrarActualizar: false
    });
  }
  delete = (dato) => {
    var verificacion = window.confirm("¿Está seguro que quiere eliminar el registro "+dato.id+" ?");
    if(verificacion){
      var i = 0;
      var list = this.state.listTableBody;
      list.map((campo) => {
        if(campo.id === dato.id){
          list.splice(i,1);
        }
        i++;
      });
      this.setState({
        listTableBody: list
      });
    }
  }


  ageCalculate = (fechaNacimiento) => {
    var fechaActual =  new Date();
    var fechaN = new Date(fechaNacimiento);
    var edad = fechaActual.getFullYear() - fechaN.getFullYear();
    var meses = fechaActual.getMonth() - fechaN.getMonth();

    if(meses < 0 || (meses === 0 && fechaActual.getDate() < fechaN.getDate())){
      edad--;
    }
    return edad;
  }

  onChange = async e => {
    e.persist();
    await this.setState({busqueda: e.target.value});
    this.filterElements();
  }

  filterElements = () => {
    var search = this.state.listTableBody.filter( item =>{
      if(item.nombre.toLowerCase().normalize('NFD').replace(/([aeio])\u0301|(u)[\u0301\u0308]/gi,"$1$2").includes(this.state.busqueda)
        || item.telefono.toString().includes(this.state.busqueda)
        || item.correoElectronico.toString().normalize('NFD').replace(/([aeio])\u0301|(u)[\u0301\u0308]/gi,"$1$2").includes(this.state.busqueda) ){
        return item;
      }
    });
    this.setState({
      listContactos: search
    });
  }

  componentDidMount(){
    this.setState({ listContactos: this.state.listTableBody })
  }

 

  render() {
    const listTableHeader = [
      {
        name: 'id',
        description: 'Id',
        selector: 'id',
        sortable: true,
      },
      {
        name: 'nombre',
        description: 'Nombre',
        selector: 'nombre',
        sortable: true,
      },
      {
        name: 'telefono',
        description: 'Teléfono',
        selector: 'telefono',
        sortable: true,
      },
      {
        name: 'fechaNacimiento',
        description: 'Fecha Nacimiento',
        selector: 'fechaNacimiento',
        sortable: true,
      },
      {
        name: 'direccion',
        description: 'Dirección',
        selector: 'direccion',
        sortable: true,
      },
      {
        name: 'correoElectronico',
        description: 'Correo Electronico',
        selector: 'correoElectronico',
        sortable: true,
      },
      {
        name: 'edad',
        description: 'Edad',
        selector: 'edad',
        sortable: true,
        cell: (row) =><div>{this.ageCalculate(row.fechaNacimiento)}</div>
      },
      {
        name: 'editar',
        description: 'Editar',
        selector: 'editar',
        cell: (row) => <Button className="btn btn-success btn-sm mr-1" onClick={()=>this.showModalUpdate(row)}>Editar</Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
      {
        name: 'eliminar',
        description: 'Editar',
        selector: 'eliminar',
        cell: (row) => <Button className="btn btn-danger btn-sm  mr-1" onClick={()=>this.delete(row)}>Eliminar</Button>,
        ignoreRowClick: true,
        allowOverflow: true,
        button: true,
      },
    ];
    return(
      <div className="container mt-4">
        <div className="row">
          <div className="col-12">
            <nav className="navbar navbar-expand-lg navbar-dark  bg-dark">
                <a className="navbar-brand" href="#">Bienvenido</a>
            </nav>
          </div>
        </div>
        <br></br>
        <div className="row">
          <div className="col-md-6">
            <Button className="btn btn-success btn-sm mr-1" onClick={()=>this.showModalAdd()}>Agregar Contacto</Button>
          </div>
        </div>
        <div className="row">
          <div className="table-responsive col-md-12">
            <div className="barraBuscar">
            <input type="text" placeholder="Buscar Contacto" className="textField form-control" name="buscar"
            value={this.state.busqueda} onChange={this.onChange} />
            </div>
            <DataTable 
              columns={listTableHeader}
              data={this.state.listContactos}
              title="Contactos"
              pagination
              noDataComponent={<span>No se encontro ningun registro</span>}
            />
          </div>
        </div>
        <Modal show={this.state.mostrarAgregar}>
          <ModalHeader>
            <div className="form-group">
              <h4>Agregar Contacto</h4>
            </div>
          </ModalHeader>
          <ModalBody>
          <form >
            <div className="form-group">
                <label htmlFor="id">Id:</label>
                <input id="id" name="id" type="text" readOnly className="form-control" value={this.state.listTableBody.length+1}
                onChange={this.handleChange}/>
            </div>

            <div className="form-group">
                <label htmlFor="nombre">Nombre:</label>
                <Input id="nombre" name="nombre" type="text" className="form-control"
                placeholder="Ingrese el nombre del contacto" onChange={this.handleChange} invalid={this.state.invalidNombre}/>
                <FormFeedback>{this.state.mensajeInvalidNombre}</FormFeedback>
            </div>

            <div className="form-group">
                <label htmlFor="telefono">Teléfono:</label>
                <Input id="telefono" name="telefono" type="number" className="form-control"
                placeholder="Ingrese el número del contacto" onChange={this.handleChange} invalid={this.state.invalidTelefono}/>
                <FormFeedback>{this.state.mensajeInvalidTelefono}</FormFeedback>
            </div>

            <div className="form-group">
                <label htmlFor="fechaNacimiento">Fecha Nacimiento:</label>
                <Input id="fechaNacimiento" name="fechaNacimiento" type="date" className="form-control" onChange={this.handleChange}
                invalid={this.state.invalidFechaNacimiento}/>
                <FormFeedback>{this.state.mensajeInvalidFechaNacimiento}</FormFeedback>
            </div>

            <div className="form-group">
                <label htmlFor="direccion">Dirección:</label>
                <Input id="direccion" name="direccion" type="text" className="form-control"
                placeholder="Ingrese la dirección del contacto" onChange={this.handleChange} invalid={this.state.invalidDireccion}/>
                <FormFeedback>{this.state.mensajeInvalidDireccion}</FormFeedback>
            </div>

            <div className="form-group">
                <label htmlFor="correoElectronico">Correo Electronico:</label>
                <Input id="correoElectronico" name="correoElectronico" type="text" className="form-control"
                placeholder="Ingrese el correo del contacto" onChange={this.handleChange} invalid={this.state.invalidCorreoRequerido || this.state.invalidCorreo}/>
                <FormFeedback>{this.state.mensajeInvalidCorreo || this.state.mensajeInvalidCorreoRequerido}</FormFeedback>
            </div>
          </form>  
          </ModalBody>
          <ModalFooter>
            <Button className="btn btn-success" onClick={()=>this.handleSave()}>Guardar</Button>
            <Button className="btn btn-danger" onClick={()=>this.hideModalAdd()}>Cancelar</Button>
          </ModalFooter>  
        </Modal>    
        <Modal show={this.state.mostrarActualizar}>
          <ModalHeader>
            <div className="from-group">
              <h4>Actualizar Contacto</h4>
            </div>
          </ModalHeader>
          <ModalBody>
            <form >
              <div className="form-group">
                  <label htmlFor="id">Id:</label>
                  <input id="id" name="id" type="text" readOnly className="form-control" value={this.state.contactos.id}
                  onChange={this.handleChange}/>
              </div>

              <div className="form-group">
                  <label htmlFor="nombre">Nombre:</label>
                  <input id="nombre" name="nombre" type="text" className="form-control" value={this.state.contactos.nombre}
                  placeholder="Ingrese el nombre del contacto" onChange={this.handleChange}/>
              </div>

              <div className="form-group">
                  <label htmlFor="telefono">Teléfono:</label>
                  <input id="telefono" name="telefono" type="number" className="form-control" value={this.state.contactos.telefono}
                  placeholder="Ingrese el número del contacto" onChange={this.handleChange}/>
              </div>

              <div className="form-group">
                  <label htmlFor="fechaNacimiento">Fecha Nacimiento:</label>
                  <input id="fechaNacimiento" name="fechaNacimiento" type="date" className="form-control" value={this.state.contactos.fechaNacimiento}
                  onChange={this.handleChange}/>
              </div>

              <div className="form-group">
                  <label htmlFor="direccion">Dirección:</label>
                  <input id="direccion" name="direccion" type="text" className="form-control" value={this.state.contactos.direccion}
                  placeholder="Ingrese la dirección del contacto" onChange={this.handleChange}/>
              </div>

              <div className="form-group">
                  <label htmlFor="correoElectronico">Correo Electronico:</label>
                  <input id="correoElectronico" name="correoElectronico" type="email" className="form-control" value={this.state.contactos.correoElectronico}
                  placeholder="Ingrese el correo del contacto" onChange={this.handleChange}/>
              </div>
            </form>
          </ModalBody>
          <ModalFooter>
              <div className="form-goup">
                <Button className="btn btn-success" onClick={()=>this.update(this.state.contactos)}>Actualizar</Button>
                <Button className="btn btn-danger" onClick={()=>this.hideModalUpdate()}>Cancelar</Button>
              </div>
          </ModalFooter>
        </Modal>
      </div>

    );
  }
}

export default App;

